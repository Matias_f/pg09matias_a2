package com.example.pg09matias.pg09matias_repositoryapp;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * Created by pg09matias on 09/08/2017.
 */

public class RepositoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView mImageView;
    private TextView mNameTextView;
    private TextView mLastUpdatedTextView;
    private String mCurrentAvatarLink;
    private String mCurrentCommitsLink;

    public RepositoryHolder(View view)
    {
        super(view);

        mImageView = (ImageView) view.findViewById(R.id.image_view);
        mNameTextView = (TextView) view.findViewById(R.id.name_textview);
        mLastUpdatedTextView = (TextView) view.findViewById(R.id.last_updated_textview);

        view.setOnClickListener(this);
    }

    public void BindRepository(Repository currentRepository)
    {
        mNameTextView.setText(currentRepository.GetName());
        mLastUpdatedTextView.setText(currentRepository.GetLastUpdate());
        mCurrentAvatarLink = currentRepository.GetAvatarLink();
        mCurrentCommitsLink = currentRepository.GetCommitsLink();
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent(v.getContext(), Navigation.class);
        intent.putExtra("REPOSITORY_AVATAR_LINK", mCurrentAvatarLink);
        intent.putExtra("REPOSITORY_COMMITS_LINK", mCurrentCommitsLink);

        v.getContext().startActivity(intent);
        Log.i("Repository Holder", "Clicked");
    }
}
