package com.example.pg09matias.pg09matias_repositoryapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mReciclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private  Menu mMenu;
    private ArrayList<Repository> testRepositories = new ArrayList<>();
    private ArrayList<Repository> mRepositories = new ArrayList<>();
    private RecyclerAdapter mRecyclerAdapter;
    private String currentUsername;
    private String currentPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Intent intent = getIntent();
        currentUsername = intent.getStringExtra("USER_USERNAME");
        currentPassword = intent.getStringExtra("USER_PASSWORD");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getRepositories();

        mReciclerView = (RecyclerView) findViewById(R.id.recicler_view);
        mMenu = (Menu) findViewById(R.id.sign_out);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mReciclerView.setLayoutManager(mLinearLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.sign_out:
                Intent intent = new Intent(MainActivity.this, Login.class);
                MainActivity.this.startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    private void getRepositories()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        String urlString = "https://api.bitbucket.org/2.0/repositories/" + currentUsername;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlString, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                Log.i("MainActivity", "onResponse");

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("values");
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject repositoryJSON = jsonArray.getJSONObject(i);
                        String name = repositoryJSON.getString("name");
                        String lastUpdate = repositoryJSON.getString("updated_on");
                        String createdOn = repositoryJSON.getString("created_on");
                        String fullName = repositoryJSON.getString("full_name");

                        mRepositories.add(new Repository(name, lastUpdate, createdOn, fullName));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                mRecyclerAdapter = new RecyclerAdapter(mRepositories);
                mReciclerView.setAdapter(mRecyclerAdapter);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                Log.i("MainActivity", "onErrorResponse");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String credentials = currentUsername + ":" + currentPassword;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }
        };

        queue.add(stringRequest);
    }
}
