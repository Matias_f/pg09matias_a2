package com.example.pg09matias.pg09matias_repositoryapp;

import java.net.URL;
import java.util.Date;

/**
 * Created by pg09matias on 09/08/2017.
 */

public class Repository {

    private String name;
    private String lastUpdate;
    private String avatarLink;
    private String commitsLink;

    public Repository(String name, String lastUpdate, String avatarLink, String commitsLink)
    {
        this.name = name;
        this.lastUpdate = lastUpdate;
        this.avatarLink = avatarLink;
        this.commitsLink = commitsLink;
    }

    public String GetName() { return this.name; };
    public String GetLastUpdate() { return this.lastUpdate; }
    public String GetAvatarLink() { return this.avatarLink; }
    public String GetCommitsLink() { return this.commitsLink; }
}
