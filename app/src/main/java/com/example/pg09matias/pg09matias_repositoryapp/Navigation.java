package com.example.pg09matias.pg09matias_repositoryapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Navigation extends AppCompatActivity
{
    private TextView mAvatarLink;
    private TextView mCommitsLink;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Intent intent = getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        mAvatarLink = (TextView) findViewById(R.id.avatar_link_textview);
        mCommitsLink = (TextView) findViewById(R.id.commits_link_textview);

        mAvatarLink.setText(intent.getStringExtra("REPOSITORY_AVATAR_LINK"));
        mCommitsLink.setText(intent.getStringExtra("REPOSITORY_COMMITS_LINK"));
    }
}
