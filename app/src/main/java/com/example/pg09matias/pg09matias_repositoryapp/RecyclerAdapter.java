package com.example.pg09matias.pg09matias_repositoryapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by pg09matias on 09/08/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RepositoryHolder>
{
    private ArrayList<Repository> mRepositories;

    public RecyclerAdapter(ArrayList<Repository> mRepositories)
    {
        this.mRepositories = mRepositories;
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recicler_view, parent, false);
        return new RepositoryHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position)
    {
        Repository currentRepository = mRepositories.get(position);
        holder.BindRepository(currentRepository);
    }

    @Override
    public int getItemCount() { return mRepositories.size(); }
}
