package com.example.pg09matias.pg09matias_repositoryapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity implements View.OnClickListener
{
    private EditText mUsername;
    private EditText mPassword;
    private Button mConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsername = (EditText) findViewById(R.id.username_edit_text);
        mPassword = (EditText) findViewById(R.id.password_edit_text);
        mConfirm = (Button) findViewById(R.id.confirm_button);

        mConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent(v.getContext(), MainActivity.class);
        intent.putExtra("USER_USERNAME", mUsername.getText().toString());
        intent.putExtra("USER_PASSWORD", mPassword.getText().toString());

        v.getContext().startActivity(intent);
    }
}
